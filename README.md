## MiniSumo

All files required to designing and creating my minisumo robot. There are circuits schematics and PCB project (Autodesk Eagle), 3D model made using Autodesk Inventor and programs from Arduino IDE.