#include "Robot.h"

void Robot::setup()
{
  surr_vision.setupSensors();
  board_vision.setupOptoCouplers();
  motors.setupMotors();
}

void Robot::collectAllData()
{
  surr_vision.getDistances();
  board_vision.getOptoReads();
}

void Robot::makeDecision()
{
  // Search for obstacles
  if(surr_vision.detectObstacle(70))
  {
    motors.stopMotors();
    motors.goBackward(300);
  }
  else
    motors.goForward(300);
}

void Robot::move(double velocity, double omega)
{
  omega = omega*PI/180;

  double left_wheel = velocity - (WHEELS_L * omega)/2;
  double right_wheel = velocity + (WHEELS_L * omega)/2;

  if(left_wheel < 0)
    left_wheel = LINEAR_COEF * left_wheel - OFFSET;
  else
    left_wheel = LINEAR_COEF * left_wheel + OFFSET;
  if(right_wheel < 0)
    right_wheel = LINEAR_COEF * right_wheel - OFFSET;
  else
    right_wheel = LINEAR_COEF * right_wheel + OFFSET;

  motors.moveMotors(left_wheel, right_wheel);
}

int Robot::findEnemy(int range)
{
  // Check if somewhere is enemy
  int sensor = -1;
  surr_vision.getDistances();
  for(int i = 0; i < 4; ++i)
  {
    if(surr_vision.clear_distances[i] <= range)
    {
      sensor = i;
      break;
    }
  }

  return sensor;
}

void Robot::checkWhiteLine()
{
  board_vision.getOptoReads();

  bool first_opt = (board_vision.opto_reads[0] < 3000) || (board_vision.opto_reads[0] > 3900);
  bool second_opt = (board_vision.opto_reads[1] < 3000) || (board_vision.opto_reads[1] > 3900);

  if(first_opt && !second_opt)
  {
    move(-30, 0);
    delay(200);
    move(0, 360);
    delay(300);
    move(30, 0);
  }
  else if(!first_opt && second_opt)
  {
    move(-30, 0);
    delay(200);
    move(0, -360);
    delay(300);
    move(30, 0);
  }
  else if(first_opt || second_opt)
  {
    stop();
    move(-30, 0);
    delay(200);
    move(0, 360);
    delay(500);
    move(20, 0);
  }
}

void Robot::chase(int range)
{
  while(true)
  {
    surr_vision.getDistances();
    // Left front sensor detects enemy
    if((surr_vision.clear_distances[1] <= range) && !(surr_vision.clear_distances[2] <= range))
      move(20, -2);
    // Right front sensor detects enemy
    else if(!(surr_vision.clear_distances[1] <= range) && (surr_vision.clear_distances[2] <= range))
      move(20, 2);
    // Both detects enemy
    else if((surr_vision.clear_distances[1] <= range) && (surr_vision.clear_distances[2] <= range))
      move(20, 0);
    // None of them detects
    else
    {
      move(10, 0);
      break;
    }
    //checkWhiteLine();
  }
}

void Robot::sendDataOnSerial()
{
  surr_vision.sendDataOnSerial();
  board_vision.sendDataOnSerial();
  Serial.print("\n");
}


