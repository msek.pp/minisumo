#include "Robot.h"

#define LED 9
#define BUTTON 0

bool button_state = false;
bool enemy_detected = false;
uint32_t start_time;

void waitForButtonPressed();
void blinkLED(int blink, int time = 300);

Robot minisumo;

void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  digitalWrite(LED, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  // Serial port init
  Serial.begin();
  // Setup all robot periphery
  minisumo.setup();

  start_time = millis();
}

void loop()
{
  waitForButtonPressed();

  // Seek the enemy in given range
  int sensor = 0;
  
  sensor = minisumo.findEnemy(300);

  if(sensor == -1)
  // If no sensor returns smaller value, then rotate
    minisumo.move(0, 150);
  else if(sensor == 0)
    // If left sensor detects, quick turn left and break the loop
    minisumo.move(20, -150);
  else if(sensor == 1 || sensor == 2)
  {
    // If front sensors detect, stop and break the loop
    minisumo.move(20, 0);
    minisumo.chase(300);
  }
  else if(sensor == 3)
    // If right sensor detects, quick turn right and break the loop
    minisumo.move(20, 150);

  //minisumo.checkWhiteLine();
}

void waitForButtonPressed()
{
  while(!button_state)
  {
    if(millis() - start_time >= 300)
    {
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      start_time = millis();
    }

    if(digitalRead(BUTTON) == LOW)
    {
      button_state = true;
      delay(500);
      start_time = millis();
    }
  }
}

void blinkLED(int blinks, int time)
{
  for(int i = 0; i < blinks; ++i)
  {
    digitalWrite(LED, HIGH);
    delay(time);
    digitalWrite(LED, LOW);
    delay(time);
  }
}




