#ifndef ROBOT_H
#define ROBOT_H

#include "Vision.h"
#include "OptoCouplers.h"
#include "DCMotors.h"

#define WHEELS_L 8.6
#define LINEAR_COEF 9.398107366
#define OFFSET 7.395194838

class Robot
{
  private:
    Vision surr_vision;
    OptoCouplers board_vision;
    DCMotors motors;

  public:
    Robot() { }
    void setup();
    void collectAllData();
    void makeDecision();
    void move(double velocity, double omega);
    void moveBack(uint16_t velocity) { motors.goBackward(velocity); }
    void moveForward(uint16_t velocity) { motors.goForward(velocity); }
    void stop() { motors.stopMotors(); }
    void sendDataOnSerial();

    int findEnemy(int range);
    void checkWhiteLine();
    void chase(int range);
};

#endif // ROBOT_H