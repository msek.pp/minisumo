#include "Motor.h"

void Motor::setupMotor(pin_size_t input1, pin_size_t input2, pin_size_t pwm)
{
  motor_velocity = 0;
  direction = state::STOP;
  motor_signals[0] = input1;
  motor_signals[1] = input2;
  pwm_pin = pwm;

  pinMode(motor_signals[0], OUTPUT);
  pinMode(motor_signals[1], OUTPUT);
  pinMode(pwm_pin, OUTPUT);

  digitalWrite(motor_signals[0], LOW);
  digitalWrite(motor_signals[1], LOW);
  analogWrite(pwm_pin, 0);
}

void Motor::move(int direction, int velocity)
{
  if(direction == state::FORWARD)
  {
    digitalWrite(motor_signals[0], HIGH);
    digitalWrite(motor_signals[1], LOW);
    direction = state::FORWARD;
  }
  else if(direction == state::BACKWARD)
  {
    digitalWrite(motor_signals[0], LOW);
    digitalWrite(motor_signals[1], HIGH);
    direction = state::BACKWARD;
  }

  analogWrite(pwm_pin, velocity);
  motor_velocity = velocity;
}

void Motor::stop()
{
  digitalWrite(motor_signals[0], LOW);
  digitalWrite(motor_signals[1], LOW);
  analogWrite(pwm_pin, 0);

  motor_velocity = 0;
  direction = state::STOP;
}