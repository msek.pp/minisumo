#ifndef VISION_H
#define VISION_H

// Library for TOF sensors
#include "TOFSensor.h"
// Pins used for turning on/off the sensors
#define XSHUT_LEFT 10
#define XSHUT_FRONT1 11
#define XSHUT_FRONT2 2
#define XSHUT_RIGHT 3
// I2C pins
#define I2C0_SDA 12
#define I2C0_SCL 13
#define I2C1_SDA 6
#define I2C1_SCL 7
// Number of sensors
#define SENSORS 4
#define TIMEOUT 500
#define AVERAGE 3

extern uint8_t addresses[];

class Vision
{
  private:
    TOFSensor tof_sensors[SENSORS];

    void setupI2C();
    void initSensors();
    void setSensorsTimeout(int timeout = TIMEOUT);
    void startMeasure();
    void restartSensors();
  
  public:
    uint16_t distances[SENSORS][AVERAGE];
    uint16_t clear_distances[SENSORS];
    Vision() { }

    void setupSensors();
    void getDistances();
    uint16_t returnSensorDist(int index) { return tof_sensors[index].returnMeasure(); }
    bool detectObstacle(uint16_t range);
    void sendDataOnSerial();
};

#endif // VISION_H
