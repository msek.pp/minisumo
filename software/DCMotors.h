#include "Motor.h"

class DCMotors
{
  private:
    Motor motors[2];

  public:
    uint16_t motors_velocity[2];

    DCMotors() { }
    void setupMotors();
    void goForward(uint16_t velocity);
    void goBackward(uint16_t velocity);
    void stopMotors();
    void getVelocities();
    void moveMotors(int velocity_1, int velocity_2);
};