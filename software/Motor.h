#ifndef MOTOR_H
#define MOTOR_H

#include <stdio.h>
#include <Arduino.h>

// H-bridge inputs for each motor
#define INA1 18
#define INA2 17
#define INB1 19
#define INB2 20
// PWM signals for each motor
#define PWMA 16
#define PWMB 21

enum state {FORWARD, BACKWARD, STOP};

class Motor
{
  private:
    uint16_t motor_velocity;
    int direction;
    pin_size_t motor_signals[2];
    pin_size_t pwm_pin;

  public:
    Motor() { };
    void setupMotor(pin_size_t input1, pin_size_t input2, pin_size_t pwm);
    void move(int direction, int velocity);
    void stop();

    uint16_t returnVelocity() { return motor_velocity; }
};

#endif // MOTOR_H